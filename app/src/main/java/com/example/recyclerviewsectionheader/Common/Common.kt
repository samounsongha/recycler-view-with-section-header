package com.example.recyclerviewsectionheader.Common

import com.example.recyclerviewsectionheader.Model.*
import java.util.*
import kotlin.collections.ArrayList

object Common {
    val VIEWTYPE_GROUP = 0
    val VIEWTYPE_PERSON = 1
    val RESULT_CODE = 1000

    var alphabet_available:MutableList<String> = ArrayList()

//    This function will short list by alphabet

    fun sortList(list:ArrayList<Person>) : ArrayList<Person> {
        list.sortWith(Comparator {
                person1,person2 -> person1!!.name!!.compareTo(person2!!.name!!)
        })
        return list
    }

//    This function will add alphabet to list

    fun addAlphabet(list:ArrayList<Person>) : ArrayList<Person> {
        var i:Int = 0
        val customeList = ArrayList<Person>()
        val firstMember = Person()
        firstMember.name = list[0].name!![0].toString() //get first character
        firstMember.viewType = VIEWTYPE_GROUP // Set view type is header
        alphabet_available.add(list[0].name!![0].toString()) //add to available list

        customeList.add(firstMember)
        i = 0
        while (i < list.size -1) {
            val person = Person()
            val name1 = list[i].name!![0]
            val name2 = list[i+1].name!![0]

            if (name1 == name2) {
                list[i].viewType = VIEWTYPE_PERSON
                customeList.add(list[i])
            }else {
                list[i].viewType = VIEWTYPE_PERSON
                customeList.add(list[i])
                person.name = name2.toString()
                person.viewType = VIEWTYPE_GROUP
                alphabet_available.add(name2.toString())
                customeList.add(person)
            }
            i++
        }
        list[i].viewType = VIEWTYPE_PERSON
        customeList.add(list[i])
        return customeList
    }

//    This function will return index of name in list

    fun findPositionWithName(name:String, list:ArrayList<Person>) : Int {
        for (i in list.indices)
            if (list[i].name == name)
                return i
        return -1

    }

//    Generate list from a-z
    fun genAlphabetList():ArrayList<String> {
        val result = ArrayList<String>()
        for (i in 65..90) //65 in ASCII CODE = A, 90 = Z
            result.add((i.toChar()).toString())
        return result
    }

//    Create Fake Name Data
    fun genPersonGroup() : ArrayList<Person> {
        val personList = ArrayList<Person>()

        var person = Person("Sam Oun","Android Developer", -1)
        personList.add(person)

        person = Person("Ramekh","IOS Developer",-1)
        personList.add(person)

        person = Person("Chaom","Infrastructure", -1)
        personList.add(person)

        person = Person("Phirom","IOS Developer", -1)
        personList.add(person)

        person = Person("Samak","Android Developer",-1)
        personList.add(person)

        person = Person("Khemra","General manager",-1)
        personList.add(person)

        person = Person("Friday","Project Manager", -1)
        personList.add(person)

        person = Person("Rithy","Android Developer",-1)
        personList.add(person)

        person = Person("Apple","Best Product",-1)
        personList.add(person)
        return personList
    }
}