package com.example.recyclerviewsectionheader.Adapter

import android.graphics.Color
import android.graphics.Color.GREEN
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.amulyakhare.textdrawable.TextDrawable
import com.example.recyclerviewsectionheader.Common.Common
import com.example.recyclerviewsectionheader.Interface.IOnAlphabetItemClickListener
import com.example.recyclerviewsectionheader.R


class AlphabetAdapter : RecyclerView.Adapter<AlphabetAdapter.MyViewHolder>() {

    internal var alphabetList : List<String>
    internal lateinit var iOnAlphabetItemClick: IOnAlphabetItemClickListener

    init {
        alphabetList = Common.genAlphabetList()
    }

    fun setAlphabetClick(iOnAlphabetItemClickListener: IOnAlphabetItemClickListener) {
        this.iOnAlphabetItemClick = iOnAlphabetItemClickListener
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyViewHolder {
        val itemView = LayoutInflater.from(p0.context).inflate(R.layout.alphabet_item,p0,false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return alphabetList.size
    }

    override fun onBindViewHolder(p0: MyViewHolder, p1: Int) {
        val drawable:TextDrawable
        val availablePosition = Common.alphabet_available.indexOf(alphabetList[p1])
        if (availablePosition != -1) {
            // If caracter avaiable in list => Green
            drawable = TextDrawable.builder().buildRound(alphabetList[p1], Color.GREEN)
        }else {
            //Otherwise
            drawable = TextDrawable.builder().buildRound(alphabetList[p1], Color.GRAY)
        }
        p0.alphabet_img.setImageDrawable(drawable)
        p0.itemView.setOnClickListener {
            iOnAlphabetItemClick.onAlphabetItemClick(alphabetList[p1],availablePosition)
        }
    }

    class MyViewHolder(itemView:View) :RecyclerView.ViewHolder(itemView) {
        internal var alphabet_img:ImageView
        init {

            alphabet_img = itemView.findViewById(R.id.alphabet_item) as ImageView
        }
    }
}